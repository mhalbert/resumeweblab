var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

router.get('/add/selectuser', function(req, res){
    account_dal.getAll(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'account': result});
        }
    });
});

router.get('/add', function(req, res) {
    if(req.query.account_id == null)
    {
        res.send('Account ID must be provided');
    }
    else
    {
        resume_dal.getSchool(req.query.account_id, function(err, school){
            if(err)
            {
                res.send('resume_dal.getSchool failed');
            }
            else
            {
                resume_dal.getCompany(req.query.account_id, function(err, company) {
                    if(err)
                    {
                        res.send('resume_dal.getCompany failed');
                    }
                    else
                    {
                        resume_dal.getSkill(req.query.account_id, function(err, skill) {
                            if(err)
                            {
                                res.send('resume_dal.getSkill failed');
                            }
                           else
                            {
                                res.render('resume/resumeAdd', {
                                    'account': req.query.account_id, 'school': school, 'skill': skill, 'company': company
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

router.post('/insert', function(req, res) {
    // simple validation
    if(req.body.resume_name == "") {
        res.send('Resume Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                account_dal.getInfo(req.body.account_id, function(err, account) {
                    if (err){
                        res.send("Failed in get account info!")
                    }
                    else {
                        resume_dal.edit(resume_id, function(err, resume) {
                            if (err) {
                                res.send("Failed in get resume info!")
                            }
                            else {
                                res.render('resume/resumeUpdate',
                                    { 'school': account[0], 'company': account[1], 'skill': account[2],
                                        'resume': resume[0][0], 'resume_school': resume[1], 'resume_company': resume[2],
                                        'resume_skill': resume[3], 'was_successful': true
                                    });
                            }
                        });
                    }
                });

            }
        });

    }
});

router.get('/edit', function(req, res) {
    if(req.query.resume_id == null || req.query.account_id == null){
        res.send('Resume_id and Account_id are required')
    }
    else
    {
        account_dal.getInfo(req.query.account_id, function(err, account){
            if(err){
                res.send('Failed in getting account info');
            }
            else
            {
                resume_dal.edit(req.query.resume_id, function(err, resume){
                    if(err)
                    {
                        res.send('Failed in getting resume info');
                    }
                    else{
                        res.render('resume/resumeUpdate',{
                            'school': account[0], 'company': account[1], 'skill': account[2],
                            'resume': resume[0][0], 'resume_school': resume[1], 'resume_company': resume[2], 'resume_skill': resume[3]
                        });

                    }
                });
            }
        });
    }
});

router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
        if(err){
            res.send(err);
        }
        else{
            resume_dal.getAll(function(err, result){
               if(err){
                   res.send(err);
               }
               else{
                   res.render('resume/resumeViewAll',{
                       'result' : result, 'resume_name': req.body.resume_name, 'was_successful': true
                   });
               }
            });
        }
    });
});


module.exports = router;