var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT * FROM account ' +
        'WHERE account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

//declare the function so it can be used locally
var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSchoolData = [];

    if (schoolIdArray.constructor === Array) {
        for (var i=0; i < schoolIdArray.length; i++) {
            accountSchoolData.push([account_id, schoolIdArray[i]])
        }
    }
    else {
        //companyAddressData.push([company_id, params.address_id]);
        accountSchoolData.push([account_id, schoolIdArray]);
    }
    connection.query(query, [accountSchoolData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.accountSchoolInsert = accountSchoolInsert;

//declare the function so it can be used locally
var accountCompanyInsert = function(account_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            accountCompanyData.push([account_id, companyIdArray[i]]);
        }
    }
    else {
        accountCompanyData.push([account_id, companyIdArray]);
    }

    connection.query(query, [accountCompanyData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.accountCompanyInsert = accountCompanyInsert;

//declare the function so it can be used locally
var accountSkillInsert = function(account_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountSkillData.push([account_id, skillIdArray[i]]);
        }
    }
    else {
        accountSkillData.push([account_id, skillIdArray]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.accountSkillInsert = accountSkillInsert;

exports.insert = function(params, callback) {

    //  INSERT THE ACCOUNT
    var query = 'INSERT INTO account (email,first_name, last_name) VALUES (?,?,?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {
        // THEN USE THE INSERT_ID THAT RETURNED AS ACCOUNT_ID
        var account_id = result.insertId;
        if (params.school_id != null) {
            accountSchoolInsert(account_id, params.school_id, function (err, result) {
            });
        }
        if (params.company_id != null) {
            accountCompanyInsert(account_id, params.company_id, function (err, result) {
            });
        }
        if (params.skill_id != null) {
            accountSkillInsert(account_id, params.skill_id, function (err, result) {
            });
        }

        callback(err, account_id);

    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete account_shool entries for this resume
        accountSchoolDelete(params.account_id, function(err, result){
            if(params.school_id != null) {
                //insert account_school ids
                accountSchoolInsert(params.account_id, params.school_id, function(err, result) {
                });
            }
        });

        //delete resume_company entries for this resume
        accountCompanyDelete(params.resume_id, function(err, result){
            if(params.company_id != null) {
                //insert company_address ids
                accountCompanyInsert(params.account_id, params.company_id, function(err, result){

                });
            }
        });

        //delete resume_skill entries for this resume
        accountSkillDelete(params.account_id, function(err, result){
            if(params.skill_id != null) {
                //insert  ids
                accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                });
            }
        });
        callback(err, params.account_id);
    });


};

//declare the function so it can be used locally
var accountSchoolDelete = function(account_id, callback){
    var query = 'DELETE FROM account_school WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSchoolDelete = accountSchoolDelete;


//declare the function so it can be used locally
var accountCompanyDelete = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountCompanyDelete = accountCompanyDelete;

//declare the function so it can be used locally
var accountSkillDelete = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSkillDelete = accountSkillDelete;

exports.edit = function(account_id, callback) {
    var query = 'SELECT * FROM account ' +
        'WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
/*
DROP PROCEDURE IF EXISTS account_getinfo;
DELIMITER //
CREATE PROCEDURE account_getinfo(_account_id int)
BEGIN
SELECT a.account_id, s.school_id, s.school_name
FROM account a
JOIN account_school ac ON a.account_id = ac.account_id
JOIN school s ON s.school_id = ac.school_id
WHERE a.account_id = _account_id;

SELECT a.account_id, ac.company_id, c.company_name
FROM account a
JOIN account_company ac ON a.account_id = ac.account_id
JOIN company c ON c.company_id = ac.company_id
WHERE a.account_id = _account_id;



SELECT a.account_id, s.skill_id, s.skill_name
FROM account a
JOIN account_skill ak ON a.account_id = ak.account_id
JOIN skill s ON s.skill_id = ak.skill_id
WHERE a.account_id = _account_id;


SELECT * FROM account
WHERE account_id = _account_id;

END //
DELIMITER ;
*/
exports.getInfo = function(account_id, callback){
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};