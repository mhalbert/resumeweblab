var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = 'SELECT * ' +
        'FROM resume r ' +
        'JOIN account a ON a.account_id = r.account_id ' +
        'ORDER BY first_name, last_name, resume_name ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

var resumeSchoolInsert = function(resume_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolInsert = resumeSchoolInsert;

var resumeSkillInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }
    connection.query(query, [resumeSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillInsert = resumeSkillInsert;

var resumeCompanyInsert = function(resume_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }
    connection.query(query, [resumeCompanyData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyInsert = resumeCompanyInsert;



exports.getSchool = function(account_id,callback) {
    var query = 'SELECT a.account_id, s.school_id, s.school_name ' +
        'FROM account a ' +
        'JOIN account_school ac ON a.account_id = ac.account_id ' +
        'JOIN school s ON s.school_id = ac.school_id ' +
        'WHERE a.account_id = ?';

    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getCompany = function(account_id,callback) {
    var query = 'SELECT a.account_id, ac.company_id, c.company_name ' +
        'FROM account a ' +
        'JOIN account_company ac ON a.account_id = ac.account_id ' +
        'JOIN company c ON c.company_id = ac.company_id ' +
        'WHERE a.account_id = ?';

    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getSkill = function(account_id,callback) {
    var query = 'SELECT a.account_id, s.skill_id, s.skill_name ' +
        'FROM account a ' +
        'JOIN account_skill ak ON a.account_id = ak.account_id ' +
        'JOIN skill s ON s.skill_id = ak.skill_id  ' +
        'WHERE a.account_id = ?';

    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE Resume
    var query = 'INSERT INTO resume (resume_name, account_id) VALUES (?, ?)';

    var queryData = [params.resume_name, params.account_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE INSERT_ID THAT RETURNED AS RESUME_ID
        var resume_id = result.insertId;

        if (params.school_id != null) {
            resumeSchoolInsert(resume_id, params.school_id, function (err, result) {
            });
        }
        if (params.company_id != null) {
            resumeCompanyInsert(resume_id, params.company_id, function (err, result) {
            });
        }
        if (params.skill_id != null) {
            resumeSkillInsert(resume_id, params.skill_id, function (err, result) {
            });
        }

        callback(err, resume_id);

    });
};

/*

DROP PROCEDURE IF EXISTS resume_getinfo;
DELIMITER //
CREATE PROCEDURE resume_getinfo(_resume_id int)
BEGIN
	SELECT * FROM resume where resume_id = _resume_id;

    SELECT * FROM resume_school WHERE resume_id = _resume_id;

	SELECT * FROM resume_company WHERE resume_id = _resume_id;

	SELECT * FROM resume_skill WHERE resume_id = _resume_id;

END //
DELIMITER ;

 */

exports.edit = function(resume_id, callback){
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//declare the function so it can be used locally
var resumeSchoolDelete = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolDelete = resumeSchoolDelete;

var resumeCompanyDelete = function(resume_id, callback){
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyDelete = resumeCompanyDelete;

var resumeSkillDelete = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillDelete = resumeSkillDelete;


exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        resumeSchoolDelete(params.resume_id, function(err, result){

            if(params.school_id != null) {
                //insert school_id
                resumeSchoolInsert(params.resume_id, params.school_id, function(err, result){
                    if(err){
                        callback(err, result);
                    }

                });
            }

        });

        resumeCompanyDelete(params.resume_id, function(err, result){

            if(params.company_id != null) {
                //insert company_id
                resumeCompanyInsert(params.resume_id, params.company_id, function(err, result){
                    if(err)
                    {
                        callback(err, result);
                    }

                });
            }

        });

        resumeSkillDelete(params.resume_id, function(err, result){

            if(params.skill_id != null) {
                //insert skill_id
                resumeSkillInsert(params.resume_id, params.skill_id, function(err, result){
                    if(err)
                    {
                        callback(err, result);
                    }

                });
            }
        });
        callback(err,result);
    });
};


